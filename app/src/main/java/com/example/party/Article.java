package com.example.party;

import javax.annotation.Nonnull;

public class Article {
    @Nonnull
    public final String text;

    public Article(String text) {
        this.text = text;
    }
}

package com.example.party;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import junit.framework.Assert;

import javax.annotation.Nonnull;

public class ArticleViewHolder extends RecyclerView.ViewHolder {
    @Nonnull
    public final TextView text;

    @Nonnull
    public final ImageView image;

    @Nonnull
    private final View root;

    public ArticleViewHolder(View itemView) {
        super(itemView);

        root = itemView;

        text = find(R.id.text);
        image = find(R.id.image);
    }

    private <T extends View> T find(int id) {
        @SuppressWarnings("unchecked")
        T view = (T) root.findViewById(id);
        Assert.assertNotNull(view);
        return view;
    }
}

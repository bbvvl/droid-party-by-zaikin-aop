package com.example.party;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import javax.annotation.Nullable;

import static junit.framework.Assert.assertNotNull;

public class MainActivity extends Activity {

    @Nullable
    private Adapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new Adapter(this, new ArticleGenerator(this).generate(100));

        setContentView(R.layout.activity_main);

        final RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler);
        assertNotNull(recycler);

        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        setupContextMenu(recycler);
    }

    private void setupContextMenu(final RecyclerView recycler) {
        registerForContextMenu(recycler);

        final GestureDetector detector = new GestureDetector(this,
                new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent e) {
                MainActivity.this.openContextMenu(recycler);
            }
        });

        recycler.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                detector.onTouchEvent(e);
                return false;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        assertNotNull(adapter);

        switch (item.getItemId()) {
            case R.id.action_shape:
                adapter.setLayoutType(R.layout.article_shape);
                return true;
            case R.id.action_cardview:
                adapter.setLayoutType(R.layout.article_cardview);
                return true;
            case R.id.action_9patch:
                adapter.setLayoutType(R.layout.article_9patch);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.example.party;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import junit.framework.Assert;

import java.util.List;

import javax.annotation.Nonnull;

public class Adapter extends RecyclerView.Adapter<ArticleViewHolder> {

    @Nonnull
    private final List<Article> data;

    @Nonnull
    private final LayoutInflater inflater;

    @Nonnull
    private final Drawable theCat;

    @LayoutRes
    private int layoutType = R.layout.article_shape;

    public Adapter(Activity activity, List<Article> data) {
        this.data = data;
        this.inflater = LayoutInflater.from(activity);

        Drawable drawable = activity.getResources().getDrawable(R.drawable.cat, activity.getTheme());
        Assert.assertNotNull(drawable);
        theCat = drawable;
    }

    public void setLayoutType(@LayoutRes int layoutType) {
        this.layoutType = layoutType;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return layoutType;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(viewType, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ArticleViewHolder holder, int position) {
        Article article = data.get(position);
        holder.text.setText(article.text);

        holder.image.setImageDrawable(theCat);
        holder.image.setAlpha(0f);
        holder.image.animate().setDuration(500).alpha(1f).start();
    }

    @Override
    public void onViewDetachedFromWindow(ArticleViewHolder holder) {
        super.onViewDetachedFromWindow(holder);

        Animation animation = holder.image.getAnimation();
        if (animation != null) {
            animation.cancel();
        }
        holder.image.setImageDrawable(null);
        holder.text.setText(null);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Return true if you want your view holders to be recycled even when animation is in progress.
     */
    @Override
    public boolean onFailedToRecycleView(ArticleViewHolder holder) {
        return super.onFailedToRecycleView(holder);
    }
}
